<?php

use Illuminate\Database\Seeder;

class ProyectosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('proyectos')->truncate();
        \DB::table('proyectos')->insert(
            [
                0 => [
                    "nombre" => "Proyecto Test",
                    "monto" => 3400000,
                    "cuotas" => 12
                ]
            ]
        );
    }
}

